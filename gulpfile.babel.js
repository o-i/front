import gulp from 'gulp';
import postcss from 'gulp-postcss';
import precss from 'precss';
import sourcemaps from 'gulp-sourcemaps';
import gulpif from 'gulp-if';
import del from 'del';
import newer from 'gulp-newer';
import browserSync from 'browser-sync';
import njkRender from 'gulp-nunjucks-render';
import prettify from 'gulp-prettify';

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev';
const server = browserSync.create();

gulp.task('css', () => {
  return gulp.src('src/styles/entry/**/*.css', { since: gulp.lastRun('css') })
    .pipe(newer('build/'))
    .pipe(gulpif(isDev, sourcemaps.init()))
    .pipe(postcss([
      precss
    ]))
    .pipe(gulpif(isDev, sourcemaps.write()))
    .pipe(gulp.dest('build/css/'));
});

gulp.task('html', () => {
  njkRender.nunjucks.configure({
    watch: false,
    trimBlocks: true,
    lstripBlocks: false
  });

  return gulp.src('src/pages/**/[^_]*.html', { since: gulp.lastRun('html') })
    .pipe(newer('build/'))
    .pipe(njkRender({
      path: ['src/']  // relative path to template
    }))
    .pipe(prettify({
      indent_size: 2,
      wrap_attributes: 'auto', // 'force'
      preserve_newlines: false,
      end_with_newline: true
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('assets', () => {
  return gulp.src('src/static/**', { since: gulp.lastRun('assets') })
    .pipe(newer('build/'))
    .pipe(gulp.dest('build/'));
});

gulp.task('serve', () => {
  server.init({
    server: 'build',
    tunnel: true
  });

  server.watch('build/**/*.*').on('change', server.reload);
});

gulp.task('watch', () => {
  gulp.watch('src/pages/**/*.html', gulp.series('html'));
  gulp.watch('src/styles/**/*.*', gulp.series('css'));
  gulp.watch('src/static/**/*.*', gulp.series('assets'));
});

gulp.task('clean', () => del('build'));

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('html', 'css', 'assets'))
);

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));
